<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Publicacion;

class publicacionController extends Controller
{
    /**
     * Metodo para obtener una vista
     * @return View
     */
    public function mostrarPublicaciones()
    {
        $publicaciones = Publicacion::all();
        return view('publicaciones', ['publicaciones' => $publicaciones]);
    }

    /**
     * Metodo para obtener una publicacion
     * @param string $slug
     * @return View
     */
    public function mostrarPublicacion($slug)
    {
        $publicacion = Publicacion::where('id', $slug)->first();
        return view('publicacion', ['publicacion' => $publicacion]);
    }

    /**
     * Metodo para modificar
     * @param Request $request
     * @return View
     */
    public function editarPublicacion(Request $request)
    {
        $publicacion = Publicacion::where('id', $request->id)->first();

        if (!$publicacion)
            echo json_encode($request->all());

        $publicacion->id = $request->id;
        $publicacion->unidad = $request->unidad;
        $publicacion->tema = $request->tema;
        $publicacion->descripcion = $request->descripcion;
        $publicacion->slug = $request->slug;
        $publicacion->save();

        return redirect()->route('mostrar.publicaciones');


    }
    public function eliminarPublicacion(Request $request){
        $publicacion = Publicacion::where('id', $request->id)->first();
        $publicacion->delete();
        return redirect()->route('mostrar.publicaciones');
    }


}

