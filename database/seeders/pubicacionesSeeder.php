<?php

namespace Database\Seeders;

use App\Models\Publicacion;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class pubicacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //Seeders para la tabla publicación
      $unidad = ["Unidad 1","Unidad 1","Unidad 1","Unidad 1","Unidad 1","Unidad 1","Unidad 1","Unidad 1" , "Unidad 2","Unidad 2","Unidad 2","Unidad 2","Unidad 2"];
      $tema = ["Tema 1", "Tema 2"];
      $descripcion =["Descripcion 1", "Descripcion 2", "Descripcion 3", "Descripcion 4", "Descripcion 5", "Descripcion 6", "Descripcion 7", "Desripcion 8", "Descripcion 9", "Descripcion 10","Descripcion 11", "Descripcion 12", "Descripcion 13", "Descripcion 14", "Descripcion 15", "Descripcion 16", "Descripcion 17", "Desripcion 18", "Descripcion 19", "Descripcion 20","Descripcion 21", "Descripcion 22", "Descripcion 23", "Descripcion 24", "Descripcion 25", "Descripcion 26", "Descripcion 27", "Desripcion 28", "Descripcion 29", "Descripcion 30"];
      $slug = ["Tema 1", "Tema 2", "Tema 3", "Tema 4", "Tema 5", "Tema 6", "Tema 7", "Tema 8", "Tema 9", "Tema 10","Tema 11", "Tema 12", "Tema 13", "Tema 14", "Tema 15", "Tema 16", "Tema 17", "Tema 18", "Tema 19", "Tema 20","Tema 21", "Tema 22", "Tema 23", "Tema 24", "Tema 25", "Tema 26", "Tema 27", "Tema 28", "Tema 29", "Tema 30"];

      for ($i=0; $i < count($unidad); $i++) { 
            Publicacion::create([
              'unidad' => $unidad[$i],
              'tema' => $tema[$i],
              'descripcion' => $descripcion[$i],
              'slug' => $slug[$i] 
            ]);

        }


    }
}
