<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http.equiv="X-UA-Compatible" content="ie=edge">
    <title>Publicación</title>
</head>
<body>
    <center>
	     <h1>Publicación</h1>
	    <form action="{{route('editar.publicacion')}}" method="POST">
            @csrf
            <label for="">Unidad</label>
            <input type="text" name="unidad" value="{{$publicacion->unidad}}">
            <input type="hidden" name="slugOriginal" value="{{$publicacion->slug}}">
            <label for="">Tema</label>
            <input type="text" name="tema" value="{{$publicacion->tema}}">
            
            <label for="">Descripcion</label> 
            <input type="text" name="descripcion" value="{{$publicacion->descripcion}}">
            
            <label for="">Slug</label> 
            <input type="text" name="slug" value="{{$publicacion->slug}}">
            
            <input type="submit" value="Guardar">   
        </form>   
    </center>
</body>
</html>