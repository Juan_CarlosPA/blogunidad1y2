<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http.equiv="X-UA-Compatible" content="ie=edge">
    <title>Publicaciones</title>
</head>
<body>
    <center>
	     <h1>Publicaciones</h1>
	    <table>
	         <tr>
			    <th>ID</th>
				<th>Unidad</th>
				<th>Tema</th>
				<th>Descripcion</th>
				<th>Slug</th>
				<th>Acciones</th>
			 </tr>
			    @foreach($publicaciones as $publicacion)
			     <tr>
				    <td>{{ $publicacion->id }}</td>
					<td>{{ $publicacion->unidad }}</td>
					<td>{{ $publicacion->tema }}</td>
					<td>{{ $publicacion->descripcion }}</td>
					<td>{{ $publicacion->slug }}</td>
					<td>
					   <a href="{{route('mostrar.publicacion',$publicacion->id)}}" target="_blank">Mostrar</a>
					   <a href="{{route('editar.publicacion',$publicacion->id)}}" target="_blank">Editar</a>
					   <a href="{{route('eliminar.publicacion',$publicacion->id)}}">Eliminar</a>
				    </td>
				 </tr>
			    @endforeach
		</table>
	</center>
</body>
</html>