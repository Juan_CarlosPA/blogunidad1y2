<!DOCTYPE HTML>
<!--
	Strongly Typed by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Cliente Servidor</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="css/main.css" />
	</head>
	<body class="homepage is-preload">
		<div id="page-wrapper">

			<!-- Header -->
				<section id="header">
					<div class="container">

						<!-- Logo -->
						    <h1 id="logo"><a href="index.html">BLOG</a></h1>
							<p></p>
							<h1 id="logo"><a href="index.html">Cliente - Servidor</a></h1>
							<p><b>INTEGRANTES:</b></p>
							<p>Karen Ali Mujica Zarate</p>
							<p>Clara Monserrat Vera Hernandez</p>
							<p>Juan Carlos Palma Aragon</p>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<li><a class="fa-engineer solid icon" href="index.html"><span></span></a></li>
									<li>
										
										<ul>
											<li><a href="#"></a></li>
											<li><a href="#"></a></li>
											<li><a href="#"></a></li>
											<li><a href="#"></a></li>
											<li>
												<a href="#">Phasellus consequat</a>
												<ul>
													<li><a href="#">Magna phasellus</a></li>
													<li><a href="#">Etiam dolore nisl</a></li>
													<li><a href="#">Phasellus consequat</a></li>
												</ul>
											</li>
											<li><a href="#">Veroeros feugiat</a></li>
										</ul>
									</li>
									
								</ul>
							</nav>

					</div>
				</section>

			<!-- Features -->
				<section id="features">
					<div class="container">
						<header>
							<h2>¡<strong>UNIDAD 1 y UNIDAD 2</strong>!</h2>
						</header>
						<div class="row aln-center">
							<div class="col-4 col-6-medium col-12-small">

								<!-- Feature -->
									<section>
										<a href="#" class="image featured"><img src="https://concepto.de/wp-content/uploads/2018/08/software-de-sistema-1-e1534948748523.jpg" alt="" /></a>
										<header>
											<h3></h3>
										</header>
										<p> <strong></strong>
										<a></a>
										<a></a></p>
									</section>

							</div>
							<div class="col-4 col-6-medium col-12-small">

								<!-- Feature -->
									<section>
										<a href="#" class="image featured"><img src="https://img-17.ccm2.net/GEc-LgUnfu5USOLBwZlXqlJU3Yc=/500x/acd126620da84695ac21810d948e8620/ccm-encyclopedia/Cliente_servidor.jpg" alt="" /></a>
				<header>
											<h3></h3>
										</header>
										<p><a></a><a></a></p>
									</section>

							</div>
							<div class="col-4 col-6-medium col-12-small">

								<!-- Feature -->
									<section>
										<a href="#" class="image featured"><img src="https://concepto.de/wp-content/uploads/2018/09/computadora-mainframe-e1591236289309.jpg" alt="" /></a>
										<header>
											<h3></h3>
										</header>
										<p><strong></strong><a></a></p>
									</section>
									
							</div>		
									
</body>
</html>