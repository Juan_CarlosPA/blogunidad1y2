<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\publicacionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/publicaciones',[publicacionController::class,'mostrarPublicaciones'])->name('mostrar.publicaciones');
Route::get('publicacion/{slug?}',[publicacionController::class,'mostrarPublicacion'])->name('mostrar.publicacion');
Route::post('publicacion/editar',[publicacionController::class,'editarPublicacion'])->name('editar.publicacion');
Route::get('publicacion/Eliminar/{id?}',[publicacionController::class,'eliminarPublicacion'])->name('eliminar.publicacion');